module.exports = {
    css: {
      loaderOptions: {
        scss: {
          prependData: `@import "~@/assets/scss/main.scss";`
        }
      }
    },
    chainWebpack: config => {
      const svgRule = config.module.rule('svg')
  
      svgRule.uses.clear()

      svgRule
      .test( /\.svg$/)
        
        .oneOf("inline")
        .resourceQuery(/inline/)
        .use("svg-url-loader")
          .loader("svg-url-loader")
          .end()
        .end()

        .oneOf("external")
          .use("vue-svg-loader")
          .loader("vue-svg-loader");
    }
  }